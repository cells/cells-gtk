;; -*- lisp-version: "8.1 [Windows] (Jan 2, 2008 9:44)"; cg: "1.103.2.10"; -*-

(in-package :cg-user)

(defpackage :CELLS-GTK)

(define-project :name :cells-gtk
  :modules (list (make-instance 'module :name "packages.lisp")
                 (make-instance 'module :name
                                "cells3-porting-notes.lisp")
                 (make-instance 'module :name "conditions.lisp")
                 (make-instance 'module :name "compat.lisp")
                 (make-instance 'module :name "widgets.lisp")
                 (make-instance 'module :name "layout.lisp")
                 (make-instance 'module :name "display.lisp")
                 (make-instance 'module :name "buttons.lisp")
                 (make-instance 'module :name "entry.lisp")
                 (make-instance 'module :name "tree-view.lisp")
                 (make-instance 'module :name "menus.lisp")
                 (make-instance 'module :name "dialogs.lisp")
                 (make-instance 'module :name "textview.lisp")
                 (make-instance 'module :name "addon.lisp")
                 (make-instance 'module :name "gtk-app.lisp"))
  :projects (list (make-instance 'project-module :name
                                 "gtk-ffi\\gtk-ffi"))
  :libraries nil
  :distributed-files nil
  :internally-loaded-files nil
  :project-package-name :cells-gtk
  :main-form nil
  :compilation-unit t
  :verbose nil
  :runtime-modules (list :cg-dde-utils :cg.base :cg.dialog-item
                         :cg.timer :cg.tooltip)
  :splash-file-module (make-instance 'build-module :name "")
  :icon-file-module (make-instance 'build-module :name "")
  :include-flags (list :compiler :top-level :local-name-info)
  :build-flags (list :allow-debug :purify)
  :autoload-warning t
  :full-recompile-for-runtime-conditionalizations nil
  :include-manifest-file-for-visual-styles t
  :default-command-line-arguments "+cx +t \"Initializing\""
  :additional-build-lisp-image-arguments (list :read-init-files nil)
  :old-space-size 256000
  :new-space-size 6144
  :runtime-build-option :standard
  :on-initialization 'cells-gtk::gtk-demo
  :on-restart 'do-default-restart)

;; End of Project Definition
