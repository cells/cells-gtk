;; -*- lisp-version: "8.1 [Windows] (Jan 2, 2008 9:44)"; cg: "1.103.2.10"; -*-

(in-package :cg-user)

(defpackage :TEST-GTK
  (:export #:gtk-demo))

(define-project :name :test-gtk
  :modules (list (make-instance 'module :name "test-gtk.lisp")
                 (make-instance 'module :name "test-layout.lisp")
                 (make-instance 'module :name "test-display.lisp")
                 (make-instance 'module :name "test-buttons.lisp")
                 (make-instance 'module :name "test-entry.lisp")
                 (make-instance 'module :name "test-tree-view.lisp")
                 (make-instance 'module :name "test-menus.lisp")
                 (make-instance 'module :name "test-dialogs.lisp")
                 (make-instance 'module :name "test-textview.lisp")
                 (make-instance 'module :name "test-addon.lisp"))
  :projects (list (make-instance 'project-module :name
                                 "..\\cells-gtk"))
  :libraries nil
  :distributed-files nil
  :internally-loaded-files nil
  :project-package-name :test-gtk
  :main-form nil
  :compilation-unit t
  :verbose nil
  :runtime-modules (list :cg-dde-utils :cg.base :cg.dialog-item
                         :cg.timer :cg.tooltip)
  :splash-file-module (make-instance 'build-module :name "")
  :icon-file-module (make-instance 'build-module :name "")
  :include-flags (list :local-name-info)
  :build-flags (list :allow-debug :purify)
  :autoload-warning t
  :full-recompile-for-runtime-conditionalizations nil
  :include-manifest-file-for-visual-styles t
  :default-command-line-arguments "+cx +t \"Initializing\""
  :additional-build-lisp-image-arguments (list :read-init-files nil)
  :old-space-size 256000
  :new-space-size 6144
  :runtime-build-option :standard
  :on-initialization 'test-gtk:gtk-demo
  :on-restart 'do-default-restart)

;; End of Project Definition
