#|

1. TRC is now back in the cells package. pod-utils no longer exports TRC. use pod::trc to get to it.
We could probably just drop TRC from pod-utils.

2. def-c-output is now defobserver. name change only.

3. md-value/.md-value is now value/.value

4. Use :owning option on cell slot to handle things like:

    popup
    tree-model



|#

(in-package :cells-gtk)

(export '(make-be))

(defun make-be (class &rest args)
  (md-awaken (apply 'make-instance class args)))

(defun to-be (x) (md-awaken x))