(in-package :test-gtk)

(defmodel test-buttons (vbox)
  ((nclics :accessor nclics :initform (c-in 0)))
  (:default-initargs
      :kids (c? (the-kids
                 (mk-label :text (c? (format nil "Toggled button active = ~a" 
                                       (value (fm-other :toggled-button)))))
                 (mk-hseparator)
                 (mk-label :text (c? (format nil "Check button checked = ~a" 
                                       (value (fm-other :check-button)))))
                 (mk-hseparator)
                 (mk-label :text (c? (format nil "Radio button selected = ~a" 
                                       (value (fm-other :radio-group)))))
                 (mk-hseparator)
                 (mk-label :text (c? (format nil "Button clicked ~a times" 
                                       (nclics (upper self test-buttons))))
                   :selectable t)
                 (mk-hseparator)
                 
                 (mk-hbox
                  :kids (c? (the-kids
                             (mk-button :stock :apply
                               :tooltip "Click ....."
                               :on-clicked (callback (widget event data)
                                             (incf (nclics (upper self test-buttons)))))
                             (mk-button :label "Continuable error"
                               :on-clicked (callback (widget event data)
                                             (trc "issuing continuable error" widget event)
                                             (error 'gtk-continuable-error :text "Oops!")))
                             (mk-toggle-button :md-name :toggled-button
                               :markup (c? (with-markup (:foreground (if (value self) :red :blue))
                                             "_Toggled Button")))
                             (mk-check-button :md-name :check-button				      
                               :markup (with-markup (:foreground :green)
                                         "_Check Button")))))
                 (mk-hbox
                  :md-name :radio-group
                  :kids (c? (the-kids
                             (mk-radio-button :md-name :radio-1
                               :label "Radio 1")
                             (mk-radio-button :md-name :radio-2
                               :label "Radio 2" :init t)
                             (mk-radio-button :md-name :radio-3
                               :label "Radio 3"))))))))
